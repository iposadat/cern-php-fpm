FROM php:7.2-fpm

# Set consistent timezone
ENV CONTAINER_TIMEZONE="UTC"
RUN rm -f /etc/localtime \
 && ln -s /usr/share/zoneinfo/${CONTAINER_TIMEZONE} /etc/localtime

 # Install prerequisite OS packages
RUN apt-get update -y && apt-get upgrade -y && apt-get install -y curl git mysql-client

# Install the PHP extensions we need
RUN set -ex \
  && buildDeps=' \
    libjpeg62-turbo-dev \
    libpng-dev \
    libpq-dev \
    zlib1g-dev \
    libicu-dev \
    libmemcached-dev \
  ' \
  && apt-get install -y --no-install-recommends $buildDeps \
  && pecl install -o -f redis \
  && docker-php-ext-enable redis \
  && ls -la /usr/local/etc/php/conf.d/ \
  && docker-php-ext-configure gd \
    --with-jpeg-dir=/usr \
    --with-png-dir=/usr \
  && docker-php-ext-install -j "$(nproc)" gd mbstring opcache pdo pdo_mysql zip

# RUN cd /tmp \
#   && git clone -b php7 https://github.com/php-memcached-dev/php-memcached \
#   && cd php-memcached \
#   && phpize \
#   && ./configure \
#   && make \
#   && cp /tmp/php-memcached/modules/memcached.so /usr/local/lib/php/extensions/no-debug-non-zts-20160303/memcached.so \
#   && docker-php-ext-enable memcached \
#   && ls -la /usr/local/etc/php/conf.d/

COPY ["opcache-recommended.ini","/usr/local/etc/php/conf.d/"]

#
# Composer
#

# Register the COMPOSER_HOME environment variable.
ENV COMPOSER_HOME=/root/.composer

# Add global binary directory to PATH.
ENV PATH /root/.composer/vendor/bin:$PATH

# Allow Composer to be run as root.
ENV COMPOSER_ALLOW_SUPERUSER=1

RUN curl -sS https://getcomposer.org/installer | \
    php -- --install-dir=/usr/bin/ --filename=composer

COPY composer.json ./

ENV DRUPAL_VERSION=8.x-dev

RUN composer install --no-scripts --no-autoloader && \
    composer create-project drupal-composer/drupal-project:${DRUPAL_VERSION} /var/www/drupal --stability dev --no-interaction

#
# Drush
#

RUN composer global require drush/drush:8.x

EXPOSE 9000

CMD ["php-fpm"]